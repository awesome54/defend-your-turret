@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                     @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        <p>{{session('success')}}</p>
                     </div>
                    @endif
                     <div style="" class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="mt-2 m-0 font-weight-bold text-primary float-left">List Room</h6>
                            <form method="POST" action="/create">
                                @csrf
                                <input type="text" name="name" class="float-right">
                                <button type="submit" class="btn btn-sm btn-primary float-right">Create Room</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>room</th>
                                            <th>leader</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>room</th>
                                            <th>leader</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                     @foreach($roomuser as $roomusers)
                                        <tr>
                                            <td>{{ $roomusers->room }}</td>
                                            <td>{{ $roomusers->name }}</td>
                                            <td>
                                                <center>
                                                    <div class="btn-group">
                                                        <form method="POST" action="/masuk">
                                                            @csrf
                                                            <input type="hidden" name="roomid" value="{{$roomusers->room_id}}">
                                                            <input type="hidden" name="team" value="red">
                                                            <input type="hidden" name="status" value="waiting">
                                                           <button type="submit" class="btn btn-sm btn-danger">masuk</button>
                                                        </form>
                                                    </div>
                                                </center>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
<!--                     <div id="contentContainer">
					  <div id="thing">
					  </div>
					</div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
