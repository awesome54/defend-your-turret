@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$roomname->name}}   <a href="/back" type="button" class="btn btn-sm btn-primary float-right"><i class="fa fa-add"></i>keluar</a></div>

                <div class="card-body">
                     <div style="" class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="mt-2 m-0 font-weight-bold text-danger float-left">Team Red</h6>

                            <form method="POST" action="/team">
                                @csrf
                                <input type="hidden" name="team" value="red">
                                <button type="submit" class="btn btn-sm btn-primary float-right">masuk team</button>
                            </form>
                        
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>photo</th>
                                            <th>player</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>photo</th>
                                            <th>player</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                     @foreach($room as $rooms)
                                        @if($rooms->team == 'red')
                                            <tr>
                                                <td><img class="col-lg-20 img-fluid" src="{{ asset('profile/' . $rooms->profile) }}"  alt="Image"></td>
                                                <td>{{ $rooms->name }}</td>
                                                <td>
                                                    @if($rooms->user_id == Auth::user()->id)
                                                     @if($rooms->status != 'leader')
                                                    <center>
                                                        <div class="btn-group">
                                                            <form method="POST" action="/cek">
                                                                @csrf
                                                                    <input type="hidden" name="status" value="ready">
                                                                 @if($rooms->status == 'waiting')
                                                                        <button id="cek" type="submit" class="btn btn-sm btn-danger">waiting</button>
                                                                    @else
                                                                        <button id="cek" type="submit" disabled>ready</button>
                                                                    @endif
                                                            </form>
                                                        </div>
                                                    </center>
                                                     @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="" class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="mt-2 m-0 font-weight-bold text-primary float-left">Team Blue</h6>

                            <form method="POST" action="/team">
                                @csrf
                                <input type="hidden" name="team" value="blue">
                                <button type="submit" class="btn btn-sm btn-primary float-right">masuk team</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>photo</th>
                                            <th>player</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>photo</th>
                                            <th>player</th>
                                            <th style="width: 120px;">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                     @foreach($room as $rooms)
                                        @if($rooms->team == 'blue')
                                            <tr>
                                                <td><img class="col-lg-20 img-fluid" src="{{ asset('profile/' . $rooms->profile) }}"  alt="Image"></td>
                                                <td>{{ $rooms->name }}</td>
                                                <td>
                                                    @if($rooms->user_id == Auth::user()->id)
                                                    <center>
                                                        <div class="btn-group">
                                                            <form method="POST" action="/cek">
                                                                @csrf
                                                                    <input type="hidden" name="status" value="waiting">
                                                                 @if($rooms->status == 'waiting')
                                                                        <button id="cek" type="submit" class="btn btn-sm btn-danger">waiting</button>
                                                                    @else
                                                                        <button id="cek" type="submit" class="btn btn-sm btn-danger" disabled>ready</button>
                                                                 @endif
                                                            </form>
                                                        </div>
                                                    </center>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @foreach($room as $rooms)
                        @if($rooms->user_id == Auth::user()->id)
                            @if($rooms->status == 'leader')
                            <form method="GET" action="/play">
                                @csrf
                                @foreach($room as $rooms)
                                <input type="hidden" name="status" value="playing">
                                @endforeach
                                <button type="submit"  class="btn btn-sm btn-success">PLAY!</button>
                            </form>
                            @else
                             <form method="GET" action="/play">
                                @csrf
                                <input type="hidden" name="status" value="playing">
                                <button type="submit"  class="btn btn-sm btn-success" disabled>PLAY!</button>
                            </form>
                            @endif

                        @endif
                    @endforeach

<!--                     <div id="contentContainer">
					  <div id="thing">
					  </div>
					</div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection