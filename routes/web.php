<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'RoomController@index')->name('home');
Route::post('/masuk', 'RoomController@insert')->name('masuk');
Route::get('/room', 'RoomController@room')->name('room');
Route::get('/back', 'RoomController@delete')->name('back');
Route::post('/cek', 'RoomController@ubahStatus')->name('cek');
Route::get('/play', 'RoomController@play')->name('play');
Route::post('/create', 'RoomController@createroom')->name('create');
Route::post('/team', 'RoomController@changeTeam')->name('team');
