<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use App\Room;
use App\User;
use App\Roomuser;
use File;
use Illuminate\Http\Request;

class RoomController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$roomuser = DB::table('roomusers')
                ->select('*','rooms.name as room', 'users.name','users.profile')
                ->join('rooms', 'roomusers.room_id', '=', 'rooms.id')
                ->join('users', 'roomusers.user_id', '=', 'users.id')
                ->where('roomusers.status', '=', 'leader')
                ->orderBy('roomusers.room_id', 'DESC')
                ->get();

        return view('home', ['roomuser' => $roomuser]);
    }
    public function insert(Request $request)
    {
    	$roomuser = DB::table('roomusers')
                ->select('*')
                ->where('user_id', '=', Auth::id())
                ->get();

        if($roomuser->isEmpty())
		{
		    Roomuser::create([
	            'room_id' => $request['roomid'],
	            'user_id' => Auth::id(),
	            'team' => $request['team'],
	            'status' => $request['status']
	        ]);

	    	Session::put('roomid', $request['roomid']);
	        return redirect('/room');
		 }else{
		 	Session::put('roomid', $request['roomid']);
		 	return redirect('/room');
		 }

    }

     public function room()
    {
    	$room = DB::table('roomusers')
                ->select('*','rooms.name as room', 'users.name','users.profile')
                ->join('rooms', 'roomusers.room_id', '=', 'rooms.id')
                ->join('users', 'roomusers.user_id', '=', 'users.id')
                ->where('roomusers.room_id', '=', Session::get('roomid'))
                ->orderBy('roomusers.room_id', 'DESC')
                ->get();
        $roomname = DB::table('rooms')
                ->select('name')
                ->where('id', '=', Session::get('roomid'))
                ->first();

        return view('room', ['room' => $room, 'roomname' => $roomname]);
    }

    public function delete()
    {
    	$roomuserse = DB::table('roomusers')
                ->select('*')
                ->where('user_id', '=', Auth::id())
                ->first();

        if ($roomuserse->status == 'leader') {
        	$room = DB::table('rooms')->where('id', $roomuserse->room_id)->delete();
        }
        else {

			$roomusers = DB::table('roomusers')->where('user_id', Auth::id())->delete();				

        }
		session_unset();
        return redirect('/home');
    }

    public function ubahStatus(Request $request)
    {
		 DB::table('roomusers')->where('user_id', Auth::id())->update([
            'status' => 'ready'
        ]);

        return redirect('/room');
    }

    public function play(Request $request)
    {
    	 DB::table('roomusers')->where('user_id', Auth::id())->update([
            'status' => 'playing'
        ]);
    	 Session::get('roomid');

        return redirect('/play');
    }

    public function createRoom(Request $request)
    {
    	 $this->validate($request, [
            'name' => 'required'
        ]);

    	$roomuser = DB::table('rooms')
                ->select('*')
                ->where('name', '=', $request['name'])
                ->get();

        if($roomuser->isEmpty())
		{
	    	Room::create([
	            'name' => $request['name']
	        ]);

	    	$room =  DB::table('rooms')
                ->select('id')
                ->where('name', '=', $request['name'])
                ->first();

             Roomuser::create([
	            'room_id' => $room->id,
	            'user_id' => Auth::id(),
	            'team' => 'red',
	            'status' => 'leader'
	        ]);

		    Session::put('roomid', [$room->id]);
	        return redirect('/room');
		 }else{
		 	 return redirect('/home')->withSuccess('room sudah ada !');
		 }
    }

     public function changeTeam(Request $request)
    {
		 DB::table('roomusers')->where('user_id', Auth::id())->update([
            'team' => $request['team']
        ]);

        return redirect('/room');
    }
}
