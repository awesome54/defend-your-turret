<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
		protected $table = 'rooms';
    	protected $fillable = ['name'];
    	public $timestamps = false;
    	public function roomusers()
    {
        return $this->hasMany('App\Roomuser');
    }
}
