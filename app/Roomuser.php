<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomuser extends Model
{
	protected $table = 'roomusers';
    protected $fillable = ['room_id','user_id','team','status'];
    public $timestamps = false;
    public function users(){
    	return $this->belongsTo('App\User', 'user_id','id');
	}
	public function rooms(){
    	return $this->belongsTo('App\Room', 'room_id','id');
	}
}
